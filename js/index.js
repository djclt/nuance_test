// function specific to pre-chat survey
function survey_input_verify(form) {
  var error_loc = "";

  // check input fields
  var input_values = form.getElementsByTagName("input");
  for (var i = 0; i < input_values.length; i++) {
    input = input_values[i];
    switch (input.name) {
      case "name":
        if (!input.value) 
          error_loc = "name";
        break;
      case "phone_number":
        var number = input.value;
        var phone_regex1 = /^\d{10}$/;
        var phone_regex2 = /^\(\d{3}\) \d{3}-\d{4}$/;
        var phone_regex3 = /\d{3}-\d{3}-\d{4}/;
        if (!number.match(phone_regex1) && !number.match(phone_regex2) && !number.match(phone_regex3))
          error_loc = "phone_number";
        break;
      // room to expand for more input fields
      default:
        break;
    }
    if (error_loc)
      break;
  }
  if (error_loc) 
    return error_loc;

  // check select fields
  var select_values = form.getElementsByTagName("select");
  for (var i = 0; i < select_values.length; i++) {
    select = select_values[i];
    switch (select.name) {
      case "state":
        if (!select.value) 
          error_loc = "state";
        break;
      // room to expand for more select fields
      default:
        break;
    }
    if (error_loc)
      break;
  }
  if (error_loc) 
    return error_loc;

  // check textarea fields
  var textarea_values = form.getElementsByTagName("textarea");
  for (var i = 0; i < textarea_values.length; i++) {
    textarea = textarea_values[i];
    switch (textarea.name) {
      case "desc":
        if (!textarea.value) 
          error_loc = "desc";
        break;
      // room to expand for more textarea fields
      default:
        break;
    }
    if (error_loc)
      break;
  }
  return error_loc;
}

// function specific to pre-chat survey
function survey_display_error(form, error_loc) {
  // reset form
  var form_elements = form.getElementsByClassName("client_value");
  var form_labels = form.getElementsByClassName("client_label");
  var label_error = document.getElementById("label_error_msg");
  if (label_error)
      label_error.parentNode.removeChild(label_error);

    // remove prior error class
  for (var i = 0; i < form_elements.length; i++) {
    form_elements[i].className = form_elements[i].className.replace(/\b input_error\b/,'');
    form_labels[i].className = form_labels[i].className.replace(/\b label_error\b/,'');
  }

  // add error class
  var element_id = "client_" + error_loc;
  var element = document.getElementById(element_id);
  var element_label = document.getElementById(element_id + "_label");
  element.className += " input_error"; 
  element_label.className += " label_error"; 
  switch (error_loc) {
    case "phone_number":
      element_label.innerHTML = "* Phone Number<span id='label_error_msg'> - invalid phone number</span>"
      break;
    default:
      break;
  }
}

// function specific to pre-chat survey
function survey_input_success() {
  var client_form = document.getElementById("client_form");
  client_form.innerHTML = "<h2 id='client_heading'>Thank you!</h2><span id='form_success'>Your information was submitted successfully. We will now connect you to the next available chat agent.</span>";
}

// function meant for general form submission, separated by form ID
function submit_form() {
  var form_id = this.id;
  switch (form_id) {
    case "client_submit":
      var form_element = document.getElementById("client_form");
      var error_loc = survey_input_verify(form_element);
      if (error_loc) {
        survey_display_error(form_element, error_loc);
      } else {
        survey_input_success();
      }
      break;
    // add more forms
    default: 
      break;
  }
}

function close_form() {
  var form = document.getElementById("survey");
  form.parentNode.removeChild(form);
}

document.getElementById("client_submit").addEventListener("click", submit_form);
document.getElementById("close_icon").addEventListener("click", close_form);